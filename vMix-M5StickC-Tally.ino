#include <WiFi.h>
#include <M5StickC.h>
#include <PinButton.h>
#include <WebServer.h>

#define LED_BUILTIN 10

//USER DEFINED VARIABLES -- Fill these in
#define WIFI_SSID "Pretty fly for a WiFi"
#define WIFI_PASS "@Iwouldwalk500miles#"
#define VMIX_IP "192.168.178.66"
#define VMIX_PORT 8099
int TALLY_NR = 0;
// END CONFIGURATION

WiFiClient client;
WebServer server(80);  // Object of WebServer(HTTP port, 80 is defult)

PinButton btnM5(37);

char currentState = -1;
char screen = 0;

void setup()
{
  Serial.begin(115200);
  M5.begin();
  delay(10);
  M5.Lcd.setRotation(3);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);

  M5.Lcd.setCursor(20, 20);
  M5.Lcd.println("vMix M5Stick-C Tally");
  M5.Lcd.setCursor(35, 40);
  M5.Lcd.println("by Guido Visser");
  delay(2000);

  // We start by connecting to a WiFi network
  WiFi.begin(WIFI_SSID, WIFI_PASS);

  M5.Lcd.println();
  M5.Lcd.println();
  M5.Lcd.setCursor(25, 60);
  M5.Lcd.println("Waiting for WiFi...");

  while (WiFi.status() != WL_CONNECTED) {
    M5.Lcd.print(".");
    delay(500);
  }
  cls();
  M5.Lcd.println("WiFi connected");
  M5.Lcd.println("IP address: ");
  M5.Lcd.print(WiFi.localIP());

  delay(1000);
  server.on("/", handle_root);

  server.begin();
  Serial.println("HTTP server started");
  delay(100);
  connectTovMix();
}

void cls()
{
  M5.Lcd.fillScreen(TFT_BLACK);
  M5.Lcd.setCursor(0, 0);
}

// Connect to vMix instance
boolean connectTovMix()
{
  cls();
  M5.Lcd.println("Connecting to vMix...");

  if (client.connect(VMIX_IP, VMIX_PORT))
  {
    M5.Lcd.println("Connected to vMix!");
    Serial.println("------------");

    // Subscribe to the tally events
    client.println("SUBSCRIBE TALLY");
    return true;
  }
  else
  {
    char tryCount = 0;
    cls();
    M5.Lcd.println("Could not connect to vMix");
    M5.Lcd.println("Retrying: 0/3");
    boolean retry = false;
    for (int i = 0; i < 3; ++i)
    {
      Serial.print(i);
      retry = retryConnectionvMix(i);
      if (!retry) {
        return true;
      }
    }
    cls();
    M5.Lcd.println("Couldn't connect to vMix");
    M5.Lcd.println();
    M5.Lcd.println("Please restart device");
    M5.Lcd.println("to retry");
    return false;
  }
}

boolean retryConnectionvMix(char tryCount) {
  cls();
  M5.Lcd.println("Couldn't connect to vMix");
  M5.Lcd.print("Retrying: ");
  M5.Lcd.print(tryCount);
  M5.Lcd.print("/3");
  delay(2000);
  boolean conn = connectTovMix();
  if (conn) {
    return false;
  }
  return true;
}

void setTallyProgram()
{
  digitalWrite(LED_BUILTIN, LOW);
  M5.Lcd.fillScreen(RED);
  M5.Lcd.setTextColor(WHITE, RED);
  M5.Lcd.setCursor(25, 23);
  M5.Lcd.println("LIVE");
}

void setTallyPreview() {
  digitalWrite(LED_BUILTIN, HIGH);
  M5.Lcd.fillScreen(GREEN);
  M5.Lcd.setTextColor(BLACK, GREEN);
  M5.Lcd.setCursor(40, 23);
  M5.Lcd.println("PRE");
}

void setTallyOff() {
  digitalWrite(LED_BUILTIN, HIGH);
  M5.Lcd.fillScreen(BLACK);
  M5.Lcd.setTextColor(WHITE, BLACK);
  M5.Lcd.setCursor(23, 23);
  M5.Lcd.println("SAFE");
}

// Handle incoming data
void handleData(String data)
{
  // Check if server data is tally data
  if (data.indexOf("TALLY") == 0)
  {

    char newState = data.charAt(TALLY_NR + 9);
    // Check if tally state has changed
    if (currentState != newState || screen == 1)
    {
      currentState = newState;
      showTallyScreen();
    }
  }
  else
  {
    Serial.print("Response from vMix: ");
    Serial.println(data);
  }
}

void showTallyScreen() {
  cls();
  screen = 0;
  M5.Lcd.setTextSize(5);
  switch (currentState)
  {
    case '0':
      setTallyOff();
      break;
    case '1':
      setTallyProgram();
      break;
    case '2':
      setTallyPreview();
      break;
    default:
      setTallyOff();
  }
}

void showMsg(const char* msg){
  cls();
  M5.Lcd.setTextSize(1);
  M5.Lcd.setTextColor(WHITE,BLACK);
  M5.Lcd.fillScreen(BLACK);
  M5.Lcd.println(msg);
}

void showNetworkScreen() {
  Serial.println("Showing Network screen");
  screen = 1;
  cls();
  M5.Lcd.fillScreen(TFT_BLACK);
  M5.Lcd.setTextSize(1);
  M5.Lcd.setTextColor(WHITE, BLACK);
  M5.Lcd.println("SSID:");
  M5.Lcd.println(WIFI_SSID);
  M5.Lcd.println();
  M5.Lcd.println("IP Address:");
  M5.Lcd.println(WiFi.localIP());
}


// WEBSERVER STUFF
String HTML = "<!DOCTYPE html>\
<html lang='en'>\
<head>\
 <meta charset='UTF-8'>\
  <meta name='viewport' content='width=device-width, initial-scale=1.0, shrink-to-fit=no'>\
  <title>vMix M5Stick-C Tally</title>\
  <link rel='stylesheet' type='text/css' href='style.css'>\
  <style>\
  .wrapper,input{width:100%}body,html{padding:0;margin:0}.wrapper{padding:10px;box-sizing:border-box}.wrapper h1{text-align:center}input[type=submit]{width:50px;margin:10px auto}\
  </style>\
</head>\
<body>\
  <div class='wrapper'>\
    <h1>vMix M5Stick-C Tally</h1>\
    <form action='/save' method='post' enctype='multipart/form-data' data-ajax='false'>\
      Tally Number:<br/>\
      <input type='number' value='1' />\
      <input type='submit' value='SAVE' class='btn btn-primary'>\
    </form>\
  </div>\
</body>\
</html>";

void handle_root() {
  server.send(200, "text/html", HTML);
}


void loop()
{
  server.handleClient();
  btnM5.update();
  if (btnM5.isClick()) {
    if (screen == 0) {
      showNetworkScreen();
    } else if (screen == 1) {
      showTallyScreen();
    }
  }
  while (client.available())
  {
    String data = client.readStringUntil('\r\n');
    handleData(data);
  }
}
